package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
)

// client is an ssh client.
type client struct {
	*ssh.Client
}

// config is a set of configuration to be applied to hosts.
type config struct {
	// include files with string content. Owner, Group and Mode are optional.
	// Only one of Content or Local is allowed.
	Files []struct {
		Path, Owner, Group, Mode, Content, Local string
	}
	// a list of packages to install
	Install []string
	// a list of packages to remove
	Remove []string
	// a list of services to restart
	Restart []string
}

// host is a remote server.
type host struct {
	Addr, User, Pass string
}

// loadConfig decodes config from a json file.
func loadConfig(path string) (config, error) {
	var cfg config

	f, err := os.Open(path)
	if err != nil {
		return cfg, fmt.Errorf("open config: %s", err)
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(&cfg); err != nil {
		return cfg, fmt.Errorf("decode config: %s", err)
	}

	cfgdir, _ := filepath.Split(path)
	if cfg.Files != nil && len(cfg.Files) > 0 {
		for i, file := range cfg.Files {
			if file.Local != "" {
				if file.Content != "" {
					return cfg, fmt.Errorf("%s: only one of Local and Content is allowed", file.Path)
				}
				path := filepath.Join(cfgdir, file.Local)
				b, err := ioutil.ReadFile(path)
				if err != nil {
					return cfg, err
				}
				cfg.Files[i].Content = string(b)
			}
		}
	}

	return cfg, nil
}

// loadHosts decodes a slice of hosts from a json file.
func loadHosts(path string) ([]host, error) {
	var hosts []host

	f, err := os.Open(path)
	if err != nil {
		return hosts, fmt.Errorf("open hosts: %s", err)
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(&hosts); err != nil {
		return hosts, fmt.Errorf("decode hosts: %s", err)
	}

	return hosts, nil
}

// dial creates an ssh connection to the host and returns a client.
func (h host) dial() (client, error) {
	cfg := &ssh.ClientConfig{
		User: h.User,
		Auth: []ssh.AuthMethod{
			ssh.Password(h.Pass),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), // todo: not this ;P
	}
	c, err := ssh.Dial("tcp", h.Addr+":22", cfg)
	if err != nil {
		return client{}, fmt.Errorf("failed to dial: %s", err)
	}
	return client{c}, nil
}

// apply runs commands to align the host's config with cfg.  Config elements are
// applied in the following order: package removal, package installation, file
// content, service restart.  apply returns false if any operations fail.
func (c client) apply(cfg config) bool {
	ok := true

	if cfg.Remove != nil && len(cfg.Remove) > 0 {
		pkgs := strings.Join(cfg.Remove, " ")
		cmd := fmt.Sprintf("/usr/bin/apt-get remove -y %s", pkgs)
		if !c.run(cmd, nil, nil) {
			ok = false
			log.Printf("%s: failed to remove packages: %s", c.Conn.RemoteAddr(), pkgs)
		}
	}

	if cfg.Install != nil && len(cfg.Install) > 0 {
		if !c.run("/usr/bin/apt-get update -y", nil, nil) {
			ok = false
			log.Printf("%s: failed to update package list", c.Conn.RemoteAddr())
		}

		pkgs := strings.Join(cfg.Install, " ")
		cmd := fmt.Sprintf("/usr/bin/apt-get install -y %s", pkgs)
		if !c.run(cmd, nil, nil) {
			ok = false
			log.Printf("%s: failed to install packages: %s", c.Conn.RemoteAddr(), pkgs)
		}
	}

	for _, f := range cfg.Files {
		dir, _ := filepath.Split(f.Path)
		cmd := fmt.Sprintf("/bin/mkdir -p %s", dir)
		if !c.run(cmd, nil, nil) {
			ok = false
			log.Printf("%s: failed to apply dir %s", c.Conn.RemoteAddr(), f.Path)
		}

		cmd = fmt.Sprintf("/usr/bin/tee %s", f.Path)
		if !c.run(cmd, strings.NewReader(f.Content), nil) {
			ok = false
			log.Printf("%s: failed to apply file %s", c.Conn.RemoteAddr(), f.Path)
		}
		if f.Owner != "" || f.Group != "" {
			cmd = fmt.Sprintf("/bin/chown %s:%s %q", f.Owner, f.Group, f.Path)
			if !c.run(cmd, nil, nil) {
				ok = false
				log.Printf("%s: failed to apply ownership %s", c.Conn.RemoteAddr(), f.Path)
			}
		}
		if f.Mode != "" {
			cmd = fmt.Sprintf("/bin/chmod %s %q", f.Mode, f.Path)
			if !c.run(cmd, nil, nil) {
				ok = false
				log.Printf("%s: failed to apply mode %s", c.Conn.RemoteAddr(), f.Path)
			}
		}
	}

	if cfg.Restart != nil && len(cfg.Restart) > 0 {
		for _, s := range cfg.Restart {
			cmd := fmt.Sprintf("/usr/bin/service %s restart", s)
			if !c.run(cmd, nil, nil) {
				ok = false
				log.Printf("%s: failed to restart service %s", c.Conn.RemoteAddr(), s)
			}
		}
	}

	return ok
}

// check for changes between host and config. log messages are printed for
// differences found in file contents and installed packages. check returns
// true if no changes are found.
func (c client) check(cfg config) bool {
	ok := true
	for _, f := range cfg.Files {
		cmd := fmt.Sprintf("/usr/bin/diff %s -", f.Path)
		if !c.run(cmd, strings.NewReader(f.Content), nil) {
			ok = false
			log.Printf("%s: file %s differs from config", c.Conn.RemoteAddr(), f.Path)
		}
	}

	var b bytes.Buffer
	if !c.run("/usr/bin/apt list --installed", nil, &b) {
		ok = false
		log.Printf("%s: failed to list packages", c.Conn.RemoteAddr())
	}

	ss := strings.Split(b.String(), "\n")
	pkgs := map[string]bool{}
	for _, s := range ss {
		pkgs[strings.Split(s, "/")[0]] = true
	}

	if cfg.Remove != nil && len(cfg.Remove) > 0 {
		for _, s := range cfg.Remove {
			if pkgs[s] {
				ok = false
				log.Printf("%s: package %s is not removed", c.Conn.RemoteAddr(), s)
			}
		}
	}
	if cfg.Install != nil && len(cfg.Install) > 0 {
		for _, s := range cfg.Install {
			if !pkgs[s] {
				ok = false
				log.Printf("%s: package %s is not installed", c.Conn.RemoteAddr(), s)
			}
		}
	}

	return ok
}

// run a command on a remote host, optionally attaching stdin and stdout.
// run returns true if the command succeeded.
func (c client) run(cmd string, stdin io.Reader, stdout io.Writer) bool {
	s, err := c.NewSession()
	if err != nil {
		return false
	}
	defer s.Close()

	s.Stdin = stdin
	s.Stdout = stdout
	if err := s.Run(cmd); err != nil {
		return false
	}
	return true
}

// command is a muppet subcommand.
type command int

const (
	nocmd command = iota
	check
	apply
)

func parseCommand(s string) command {
	return map[string]command{
		"check": check,
		"apply": apply,
	}[s]
}

// work runs commands on hosts read from hc. After hc is closed, it sends true on
// okc if all work was successful.
func work(cmd command, cfg config, hc chan host, okc chan bool) {
	ok := true
	for h := range hc {
		c, err := h.dial()
		if err != nil {
			log.Print(err)
			continue
		}
		switch cmd {
		case check:
			ok = ok && c.check(cfg)
		case apply:
			ok = ok && c.apply(cfg)
		}
	}
	okc <- ok
}

func main() {
	log.SetFlags(0)
	flag.Usage = func() {
		log.Printf("Usage: %s [options] <check|apply>", os.Args[0])
		flag.PrintDefaults()
	}

	var args = struct {
		cfg, hosts string
	}{}

	flag.StringVar(&args.cfg, "c", "config.json", "config file `path`")
	flag.StringVar(&args.hosts, "h", "hosts.json", "hosts file `path`")
	flag.Parse()

	cmd := parseCommand(flag.Arg(0))
	if cmd == nocmd {
		flag.Usage()
		os.Exit(2)
	}

	cfg, err := loadConfig(args.cfg)
	if err != nil {
		log.Fatal(err)
	}
	hosts, err := loadHosts(args.hosts)
	if err != nil {
		log.Fatal(err)
	}

	// use parallel workers to run commands on multiple hosts
	const workers = 2
	var (
		hc  = make(chan host)
		okc = make(chan bool)
	)

	go func() {
		for _, h := range hosts {
			hc <- h
		}
		close(hc)
	}()

	for i := 0; i < workers; i++ {
		go work(cmd, cfg, hc, okc)
	}

	// wait for the workers to finish
	ok := true
	for i := 0; i < workers; i++ {
		ok = ok && <-okc
	}

	if !ok {
		os.Exit(3)
	}
}
