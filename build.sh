#!/bin/bash

cmd='shopt -s extglob
rm -f muppet?(.exe)
for os in linux darwin windows; do
        GOOS=$os go build || exit 1
        zip -j "muppet-$os.zip" muppet?(.exe) || exit 1
        rm -f muppet?(.exe)
done
'

docker run --rm \
  -e CGO_ENABLED=0 \
  -e GOARCH=amd64 \
  -v "$PWD:/src" \
  -w /src \
  dedelala/go bash -c "$cmd"
