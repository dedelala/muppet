# muppet

Command `muppet` configures remote servers using an ssh connection.


## usage

```
muppet [-h hosts.json] [-c config.json] <check|apply>
```


## check

The `check` subcommand looks for changes between the host and the desired config.
If changes are found, `muppet` exits with code `3`.


## apply

The `apply` subcommand applies config in the following order:
1. Remove packages.
1. Install packages.
1. Create/update files.
1. Restart services.


## hosts

Hosts are defined in a json file. The `-h` flag is optional,
by default `muppet` will look for `hosts.json` in the working directory.
Hosts are specified in a list, each with an address, username and password.

```
[
  {
    "Addr": "1.1.1.1",
    "User": "chad",
    "Pass": "guest"
  },
  {
    "Addr": "1.1.1.2",
    "User": "mary",
    "Pass": "password"
  }
]
```


## config

Configuration is defined in a json file. The `-c` flag is optional,
by default `muppet` will look for `config.json` in the working directory.
A config may specify files with content, path, named owner and group, and
file mode. It may also specify a list of packages to install, a list of
packages to remove and a list of services to restart.

```
{
  "Files": [
    {
      "Path": "/root/blep",
      "Owner": "root",
      "Group": "root",
      "Mode": "440",
      "Content": "a file with words in it"
    },
    {
      "Path": "/root/dot/garbage/nyaa",
      "Local": "nyaa"
    }
  ],
  "Install": [
    "apache2"
  ],
  "Remove": [
    "rolldice"
  ],
  "Restart": [
    "dbus"
  ]
}
```

## file content

File content may be specified inline, using `Content`, or from a file using
`Local`. Local files are relative to the directory of the `config.json`.


## build

Build static executables for linux, darwin and windows using `build.sh`.
The only dependencies for building are bash and docker.

The `dedelala/muppet` docker image is created with `build-docker.sh`.

Builds depend on the `dedelala/go` docker image.


## docker

Run `muppet` from the `dedelala/muppet` docker image.

```
docker run --rm \
  -v "$PWD/config.json:/config.json" \
  -v "$PWD/hosts.json:/hosts.json" \
  dedelala/muppet <check|apply>
```

