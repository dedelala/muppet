#!/bin/bash

shopt -s extglob

rm -f muppet?(.exe)

docker run --rm \
  -e CGO_ENABLED=0 \
  -e GOARCH=amd64 \
  -e GOOS=linux \
  -v "$PWD:/src" \
  -w /src \
  dedelala/go go build

docker build -t dedelala/muppet:latest .

rm -f muppet?(.exe)
